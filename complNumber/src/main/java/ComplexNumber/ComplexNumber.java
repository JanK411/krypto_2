package ComplexNumber;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

public class ComplexNumber {
    /**
     * real number
     */
    BigDecimal re;
 
    /**
     * imaginary unit
     */
    BigDecimal im;
 
    /**
     * Creates a complex number
     *
     * @param real
     * 			Real number
     */
    public ComplexNumber(BigInteger real) {
        this(real, BigInteger.ZERO);
    }
 
    /**
     * Creates a complex number
     *
     * @param real
     *            real part
     * @param img
     *            imaginary part
     */
    public ComplexNumber(BigDecimal real, BigDecimal img) {
        this.re = real;
        this.im = img;
    }
    
    /**
     * Creates a complex number
     *
     * @param real
     *            real part
     * @param img
     *            imaginary part
     */
    public ComplexNumber(BigInteger real, BigInteger img) {
        this.re = new BigDecimal(real);
        this.im = new BigDecimal(img);
    }
    
    /**
     * Creates a complex number
     *
     * @param real
     *            real part
     * @param img
     *            imaginary part
     */
    public ComplexNumber(long real, long img) {
        this.re = BigDecimal.valueOf(real);
        this.im = BigDecimal.valueOf(img);
    }
 
    /**
     * Creates a complex number
     *
     * @param cn
     *            Complex number
     */
    public ComplexNumber(ComplexNumber cn) {
        this.re = cn.re;
        this.im = cn.im;
    }
    
    public ComplexNumber(double re, double im) {
		this.re = new BigDecimal(re);
		this.im = new BigDecimal(im);
	}

	/**
     * Adds a complex number to this complex number
     *
     * @param cn
     *            complex number that should be added
     * @return result of the addition
     */
    public ComplexNumber add(ComplexNumber cn) {
        return new ComplexNumber(this.re.add(cn.re), this.im.add(cn.im));
    }
 
    /**
     * Adds a real number to this complex number
     *
     * @param number
     *            real number that should be added
     * @return result of the addition
     */
    public ComplexNumber add(BigInteger number) {
        return this.add(new ComplexNumber(number));
    }
    
    /**
     * Substract a complex number from a ComplexNumber
     *
     * @param cn
     *            complex number to be substracted
     * @return result of the substraction
     */
    public ComplexNumber subtract(ComplexNumber cn) {
        return new ComplexNumber(this.re.subtract(cn.re), this.im.subtract(cn.im));
    }
    
    /**
     * Substract a real number from a ComplexNumber
     *
     * @param number
     *            real number to be substracted
     * @return result of the substraction
     */
    public ComplexNumber subtract(BigInteger number) {
        return this.subtract(new ComplexNumber(number));
    }
    
    /**
     * Multiply a complex number to this ComplexNumber
     *
     * @param cn
     *            complexNumber to be added
     * @return result of the multiplication
     */
    public ComplexNumber multiply(ComplexNumber cn) {
        BigDecimal re = this.re.multiply(cn.re).subtract(this.im.multiply(cn.im));
        BigDecimal im = this.im.multiply(cn.re).add(this.re.multiply(cn.im));
        return new ComplexNumber(re, im);
    }
    
    /**
     * Multiply a real number with this ComplexNumber
     *
     * @param number
     *            real number to be multiplied
     * @return result of the multiplication
     */
 
    public ComplexNumber multiply(BigInteger number) {
        return this.multiply(new ComplexNumber(number));
    }
    
    /**
     * Divide this number by a ComplexNumber
     *
     * @param cn
     *            the complex number to be divided by
     * @return the result of the division
     */
    public ComplexNumber divide(ComplexNumber cn) {
    	ComplexNumber zaehler = this.multiply(cn.getComplexConjugate());
    	BigDecimal nenner = cn.re.pow(2).add(cn.im.pow(2));
    	return new ComplexNumber(zaehler.re.divide(nenner),zaehler.im.divide(nenner));
    }
 
    /**
     * Divide this number by a real number
     *
     * @param number
     *            the real number to be divided by
     * @return the result of the division
     */
    public ComplexNumber divide(BigInteger number) {
        return this.divide(new ComplexNumber(number));
    }
    
    /**
     * Get the complex conjugate of a complex number
     * @return the complex conjugate of this
     */
    public ComplexNumber getComplexConjugate() {
    	return new ComplexNumber(this.re, this.im.negate());
    }
    
    public boolean equals(Object o) {
        if (o instanceof ComplexNumber) {
            ComplexNumber cn = (ComplexNumber) o;
            if (cn.re.equals(this.re) && cn.im.equals(this.im)) {
                return true;
            }
        }
        return false;
    }
    
    public BigDecimal abs() {
		return sqrtFromBigInteger(this.re.pow(2).add(this.im.pow(2)), 100);
    }
    
    public static BigDecimal sqrtFromBigInteger(BigDecimal A, final int SCALE) {
    	BigDecimal x0 = new BigDecimal("0");
        BigDecimal x1 = new BigDecimal(Math.sqrt(A.doubleValue()));
        while (!x0.equals(x1)) {
            x0 = x1;
            x1 = A.divide(x0, SCALE, RoundingMode.HALF_UP);
            x1 = x1.add(x0);
            x1 = x1.divide(new BigDecimal("2"), SCALE, RoundingMode.HALF_UP);

        }
        return x1;
    }
 
    public int hashCode() {
        BigDecimal re = this.re;
        BigDecimal im = this.im;
 
        // XOR erhält eine Gleichverteilung
        return re.hashCode() ^ im.hashCode();
    }
 
    public String toString() {
        return this.re + "+" + this.im + "i";
    }
    
    /**
     * @return rounds off the complexnumers imaginary and real part.
     */
    public ComplexNumber floor() {
    	return new ComplexNumber(this.re.setScale(0, RoundingMode.FLOOR),this.im.setScale(0, RoundingMode.FLOOR)); 
    }

	/**
	 * @return true if real and imaginary part of {@code this} is zero.
	 */
	public boolean isNull() {
		return re.equals(BigDecimal.ZERO) && im.equals(BigDecimal.ZERO);
	}
 
}