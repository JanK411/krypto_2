package ellipticCurve;

import java.math.BigInteger;

public class PublicKey extends Key {
	EllipticCurveState state;
	private BigInteger p;
	private BigInteger q;
	private GroupElement g;
	private GroupElement y;

	public PublicKey(String curve, BigInteger p, BigInteger q, GroupElement g, GroupElement y) {
		if (curve.equals(CURVE_WITHOUT_N)) {
			this.state = new WithNState();
		} else if (curve.equals(CURVE_WITH_N)) {
			this.state = new WithoutNState();
		} else {
			throw new Error();
		}
		this.p = p;
		this.q = q;
		this.g = g;
		this.y = y;
	}
}
