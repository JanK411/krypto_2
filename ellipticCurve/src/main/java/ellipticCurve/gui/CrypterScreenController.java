package ellipticCurve.gui;

import ellipticCurve.Key;
import ellipticCurve.NoKey;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class CrypterScreenController {

	private static final int ENCRYPTMODE = 0;
	private static final int DECRYPTMODE = 1;

	private final Key myKey = new NoKey();
	private final Key opponnetKey = new NoKey();

	@FXML
	private ToggleButton encryptModeToggleButton;

	@FXML
	private HBox keyManagerBox;

	@FXML
	private TextArea inputArea;

	@FXML
	private TextField inputSignatureField;

	@FXML
	private TextArea outputArea;

	@FXML
	private ToggleButton manageKeysToggleButton;

	@FXML
	private HBox outputSignatureHBox;

	@FXML
	private HBox inputSignatureHBox;

	@FXML
	private CheckBox signatureCheckBox;

	@FXML
	private Button encryptButton;

	@FXML
	private ToggleButton decryptModeToggleButton;

	@FXML
	private Button decryptButton;

	@FXML
	private TextField outputSignatureField;

	@FXML
	Button myCurveButton;

	@FXML
	Button opponentCurveButton;

	@FXML
	HBox myNBox;

	@FXML
	HBox opponentNBox;

	@FXML
	void clearButtonAction() {
		this.clear();
	}

	private void clear() {
		this.outputArea.clear();
		this.inputArea.clear();
		this.inputSignatureField.clear();
		this.outputSignatureField.clear();
	}

	@FXML
	void encryptButtonAction() {

	}

	@FXML
	void decryptButtonAction() {
	}

	@FXML
	void closeButtonAction() {
		this.getStage().close();
	}

	@FXML
	void generateMyKeyAction() {
		// TODO generateKey
	}

	@FXML
	void generateOpponentKeyAction() {
		// TODO generateKey
	}

	@FXML
	void takeFromAboveAction() {
		// TODO copy Key from above and save it somehow
	}

	public void init() {
		this.encryptModeToggleButton.setSelected(true);
		this.decryptButton.setVisible(false);

		this.encryptButton.visibleProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				this.decryptButton.setVisible(false);
			}
		});
		this.decryptButton.visibleProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				this.encryptButton.setVisible(false);
			}
		});
		this.encryptButton.managedProperty().bind(this.encryptButton.visibleProperty());
		this.decryptButton.managedProperty().bind(this.decryptButton.visibleProperty());

		this.keyManagerBox.visibleProperty().bind(this.manageKeysToggleButton.selectedProperty());
		this.keyManagerBox.managedProperty().bind(this.keyManagerBox.visibleProperty());
		this.manageKeysToggleButton.selectedProperty()
				.addListener((observable, oldValue, newValue) -> this.sizeToScene());
		this.inputSignatureHBox.visibleProperty().bind(this.signatureCheckBox.selectedProperty());
		this.outputSignatureHBox.visibleProperty().bind(this.signatureCheckBox.selectedProperty());

		this.myNBox.managedProperty().bind(this.myNBox.visibleProperty());
		this.opponentNBox.managedProperty().bind(this.opponentNBox.visibleProperty());

		this.myCurveButton.setText(Key.CURVE_WITHOUT_N);
		this.opponentCurveButton.setText(Key.CURVE_WITHOUT_N);
		this.myNBox.setVisible(false);
		this.opponentNBox.setVisible(false);
	}

	private void sizeToScene() {
		this.getStage().sizeToScene();
	}

	private Stage getStage() {
		return (Stage) this.keyManagerBox.getScene().getWindow();
	}

	private void updateCryptionMode(final int i) {
		if (i == DECRYPTMODE) {
			this.decryptButton.setVisible(true);
		}
		if (i == ENCRYPTMODE) {
			this.encryptButton.setVisible(true);
		}
	}

	@FXML
	public void decryptModeToggleButtonClicked() {
		this.decryptModeToggleButton.setSelected(true);
		this.encryptModeToggleButton.setSelected(false);
		this.updateCryptionMode(DECRYPTMODE);
	}

	@FXML
	public void encryptModeToggleButtonClicked() {
		this.encryptModeToggleButton.setSelected(true);
		this.decryptModeToggleButton.setSelected(false);
		this.updateCryptionMode(ENCRYPTMODE);
	}

	@FXML
	public void myCurveButtonClickedAction() {
		if (this.myCurveButton.getText().equals(Key.CURVE_WITHOUT_N)) {
			this.myCurveButton.setText(Key.CURVE_WITH_N);
			this.myNBox.setVisible(true);
		} else {
			this.myCurveButton.setText(Key.CURVE_WITHOUT_N);
			this.myNBox.setVisible(false);
		}
		this.sizeToScene();
	}

	@FXML
	public void opponentCurveButtonClickedAction() {
		if (this.opponentCurveButton.getText().equals(Key.CURVE_WITHOUT_N)) {
			this.opponentCurveButton.setText(Key.CURVE_WITH_N);
			this.opponentNBox.setVisible(true);

		} else {
			this.opponentCurveButton.setText(Key.CURVE_WITHOUT_N);
			this.opponentNBox.setVisible(false);
		}
		this.sizeToScene();
	}

}
