package ellipticCurve.gui;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class CrypterScreen extends Application {
	@Override
	public void start(final Stage primaryStage) throws Exception {
		primaryStage.initStyle(StageStyle.DECORATED);
		HBox page;
		try {
			final URL resource = this.getClass().getResource("CrypterScreen.fxml");
			final FXMLLoader loader = new FXMLLoader(resource);
			page = (HBox) loader.load();
			final Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Elliptic-Curve-Crypter");
			// primaryStage.setResizable(false);
			final CrypterScreenController controller = loader.getController();
			primaryStage.setOnCloseRequest(e -> Platform.exit());
			controller.init();
			primaryStage.sizeToScene();
			primaryStage.show();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(final String[] args) {
		javafx.application.Application.launch(CrypterScreen.class);
	}

}
