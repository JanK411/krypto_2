package ellipticCurve;

import java.math.BigInteger;

public class GroupElement {
	BigInteger left;
	BigInteger right;

	public GroupElement(BigInteger left, BigInteger right) {
		this.left = left;
		this.right = right;
	}

}
