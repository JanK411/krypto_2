package ellipticCurve;

import java.math.BigInteger;

public class PrivateKey extends PublicKey {
	private BigInteger x;

	public PrivateKey(String curve, BigInteger p, BigInteger q, GroupElement g, GroupElement y, BigInteger x) {
		super(curve, p, q, g, y);
		this.x = x;
	}

}
