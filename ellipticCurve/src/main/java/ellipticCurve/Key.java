package ellipticCurve;

import java.math.BigInteger;
import java.util.Random;

import primeGenerator.PrimeGenerator;

public abstract class Key {

	public static final String CURVE_WITHOUT_N = "y² = x³ - x";
	public static final String CURVE_WITH_N = "y² = x³ - n²x";
	private static final int BITLENGTH = 1024;

	public static PrivateKey generateKey(String curve) {

		BigInteger p = PrimeGenerator.create(BITLENGTH).generatePrime(5, 8);
		return new PrivateKey(curve, p, q, g, y, x);

	}

	public static void main(String[] args) {
		BigInteger generatePrime = PrimeGenerator.create(BITLENGTH).generatePrime(5, 8);
		System.out.println(generatePrime);
		System.out.println(generatePrime.mod(BigInteger.valueOf(8)));
		System.out.println(generatePrime.isProbablePrime(50));
	}
}
