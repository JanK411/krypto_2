package euklid;


import ComplexNumber.ComplexNumber;

public class EuklidInZi {
	public static ComplexNumber ggt(ComplexNumber a, ComplexNumber b) {
		if (a.abs().compareTo(b.abs()) < 0)
			return ggt(b, a);

		ComplexNumber g0 = new ComplexNumber(b);
		ComplexNumber g1 = new ComplexNumber(a);

		while (!g1.isNull()) {
			ComplexNumber ck = g0.divide(g1);
			ComplexNumber parkplatz = g1;
			g1 = g0.subtract(g1.multiply(ck.floor())); //TODO hier nicht floor sondern die "f-funktion" benutzen
			g0 = parkplatz;
		}
		
		return g0;
	}
	
	public static ComplexNumber ggtRekursiv(ComplexNumber a, ComplexNumber b) {
		if (a.abs().compareTo(b.abs()) < 0) {
			return computeGGTRekursiv(b,a);
		} else {
			return computeGGTRekursiv(a,b);
		}
		
	}

	private static ComplexNumber computeGGTRekursiv(ComplexNumber a, ComplexNumber b) {
		ComplexNumber g0 = new ComplexNumber(b);
		ComplexNumber g1 = new ComplexNumber(a);
		if(!g1.isNull()) {
			return g0;
		}
		ComplexNumber flooredCK = g0.divide(g1).floor();
		return computeGGTRekursiv(g0.subtract(g1.multiply(flooredCK)), g1);
	}
}
