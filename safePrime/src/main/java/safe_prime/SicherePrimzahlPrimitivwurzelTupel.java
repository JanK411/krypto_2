package safe_prime;

import java.math.BigInteger;

public class SicherePrimzahlPrimitivwurzelTupel {
	BigInteger p;
	BigInteger g;

	public SicherePrimzahlPrimitivwurzelTupel(BigInteger p, BigInteger g) {
		this.p = p;
		this.g = g;
	}
}
