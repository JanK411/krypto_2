package safe_prime;

import java.math.BigInteger;
import java.util.Random;

public class SafePrimeGenerator {
	private final static int RUNS = 50;
	private final int bitLength;

	public SafePrimeGenerator(final int bitLength) {
		this.bitLength = bitLength;
	}

	public static SicherePrimzahlPrimitivwurzelTupel generateSafePrime(final int bitLength) {
		final SafePrimeGenerator generator = new SafePrimeGenerator(bitLength);
		return generator.generateSafePrime();
	}

	private SicherePrimzahlPrimitivwurzelTupel generateSafePrime() {
		BigInteger q = null; // TODO wegmachen :P
		BigInteger p = null; // TODO wegmachen :P
		BigInteger g = null; // TODO wegmachen :P
		final Random random = new Random();

		do {
			q = BigInteger.probablePrime(bitLength - 1, random);
			p = q.multiply(BigInteger.valueOf(2)).add(BigInteger.ONE);
		} while (!p.isProbablePrime(RUNS));

		do {
			g = new BigInteger(bitLength, random).mod(p);
		} while (!(!(g.equals(BigInteger.ONE) || g.equals(p.subtract(BigInteger.ONE)))
				&& g.modPow(q, p).equals((BigInteger.valueOf(-1).mod(p)))));

		return new SicherePrimzahlPrimitivwurzelTupel(p, g);
	}
}