package safe_prime;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class SafePrimeGeneratorScreenController {

	@FXML
	private TextField bitLenField;

	@FXML
	private Button generateButton;

	@FXML
	private TextArea generatedPrimeArea;

	@FXML
	private void generateAction() {
		final SicherePrimzahlPrimitivwurzelTupel generateSafePrime = SafePrimeGenerator.generateSafePrime(Integer.valueOf(this.bitLenField.getText()));
	}

}
