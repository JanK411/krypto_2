package safe_prime;

import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Test;

public class SafePrimeGeneratorTest {

	private static int RUNS = 30;

	@Test
	public void test() {
		for (int i = 0; i < 3; i++) {
			final BigInteger p = SafePrimeGenerator.generateSafePrime(20).p;
			final BigInteger q = p.subtract(BigInteger.ONE).divide(BigInteger.valueOf(2));
			System.out.println(p);
			System.out.println(q);

			assertTrue(q.isProbablePrime(RUNS));
			assertTrue(p.isProbablePrime(RUNS));
			System.out.println("ein test durch");
		}
	}

}
